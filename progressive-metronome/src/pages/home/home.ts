import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { NativeAudio } from '@ionic-native/native-audio';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

    tempo: number;
    hits: number;
    division: number;
    timer: number;

    private hitCount: number;

    constructor(public navCtrl: NavController, private nativeAudio: NativeAudio) {
      this.tempo = 120;
      this.hits = 4;
      this.division = 4;
      this.nativeAudio.preloadSimple('down', 'assets/audio/4c#.wav').then(
          function onSuccess() { console.log("Audio loaded") },
          function onError() { console.log("Error loading audio") });
      this.nativeAudio.preloadSimple('up', 'assets/audio/4d.wav').then(
          function onSuccess() { console.log("Audio loaded") },
          function onError() { console.log("Error loading audio") });
      this.hitCount = 1;
  }
  
  onLink(url: string) {
      window.open(url);
  }

  private bpmToMs(bpm: number): number {
    return 60000 / bpm;
  }

  play(): void {
      this.hitCount = 1;
      this.tick();
  }

  stop() {
      clearTimeout(this.timer);
      this.timer = undefined;
  }

    /*
  setInterval(interval: number) {
      this.interval = interval;
  }
    */
  private tick() {
      // do something here
      if (this.hitCount == 1) {
          this.nativeAudio.play('up');
          this.hitCount++;
      } else {
          this.nativeAudio.play('down');
          if (this.hitCount < this.hits) {
              this.hitCount++;
          } else {
              this.hitCount = 1;
          }
      }

      this.timer = setTimeout(this.tick.bind(this), this.bpmToMs(this.tempo));
  }

}
